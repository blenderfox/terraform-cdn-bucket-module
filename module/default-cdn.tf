
resource "google_dns_record_set" "default-bucket-cname" {
  managed_zone = data.google_dns_managed_zone.dns-zone.name # Name of your managed DNS zone
  name         = "${var.default_bucket_name}.${var.domain}."
  type         = "CNAME"
  ttl          = 300
  rrdatas      = ["${var.default-cdn-prefix}.${var.domain}."]
  project      = var.project
}

resource "google_compute_backend_bucket" "default-cdn-backend-bucket" {
  name        = "${var.dns-prefix}-${var.default_bucket_name}-backend-bucket"
  description = "Backend bucket for serving static content through CDN For Wordpress BF"
  bucket_name = google_storage_bucket.bucket.name
  enable_cdn  = true
  project     = var.project
  # depends_on = [
  #   google_storage_default_object_access_control.assets-viewer-access
  # ]
}

resource "random_id" "default-service-url-signature" {
  byte_length = 16
}

resource "google_compute_backend_bucket_signed_url_key" "default-bucket-backend-key" {
  name           = "${var.dns-prefix}-${var.default_bucket_name}-bucket-signed-url-key"
  key_value      = random_id.default-service-url-signature.b64_url
  backend_bucket = google_compute_backend_bucket.cdn-backend-bucket.name
}

## Add delay because backend-bucket takes time to make and without this, url-map
## will fall over

resource "null_resource" "default-bucket-delay" {
  provisioner "local-exec" {
    command = "sleep ${var.sleep_time}"
  }
  triggers = {
    "bucket" = "${google_storage_bucket.default-bucket.id}"
  }
}

## Add output so the module caller and use it
output "default_bucket_service" {
  value = google_compute_backend_bucket.default-cdn-backend-bucket
}
