## Public address that the CDN will be exposed on
resource "google_compute_global_address" "cdn-public-address" {
  name         = var.public-address-name
  ip_version   = "IPV4"
  address_type = "EXTERNAL"
  project      = var.project
}

## Map a CNAME to the public address
resource "google_dns_record_set" "cdn-dns" {
  managed_zone = data.google_dns_managed_zone.dns-zone.name # Name of your managed DNS zone
  name         = "${var.default-cdn-prefix}.${var.domain}."
  type         = "A"
  ttl          = 300
  rrdatas      = [google_compute_global_address.cdn-public-address.address]
  project      = var.project
}

## Forwards traffic from the public IP to the https proxy
resource "google_compute_global_forwarding_rule" "cdn-forwarding-to-proxy" {
  name       = var.forwarding-rule-name
  target     = google_compute_target_https_proxy.cdn-https-proxy.self_link
  ip_address = google_compute_global_address.cdn-public-address.address
  port_range = "443"
  project    = var.project
}

## Managed cert for the names specified
resource "google_compute_managed_ssl_certificate" "cdn-cert" {
  provider = google-beta
  project  = var.project

  name = var.cert-name

  managed {
    # domains = [local.domain] # Single domain to put on cert
    domains = var.domains # List of domains to put on cert
  }
}

## Proxies traffic to the routing table
resource "google_compute_target_https_proxy" "cdn-https-proxy" {
  provider         = google-beta
  name             = var.https-proxy-name
  url_map          = google_compute_url_map.routing.id
  ssl_certificates = [google_compute_managed_ssl_certificate.cdn-cert.self_link]
}

resource "google_compute_url_map" "routing" {
  name            = var.routing-map-name
  description     = "CDN URL map to various buckets"
  default_service = google_compute_backend_bucket.default-cdn-backend-bucket.self_link
  project         = var.project

  dynamic "host_rule" {
    for_each = var.host_rules
    content {
      hosts        = host_rule.value.hosts
      path_matcher = host_rule.value.path_matcher
    }
  }

  dynamic "path_matcher" {
    for_each = var.path_matcher
    content {
      name            = path_matcher.value.name
      default_service = path_matcher.value.default_service
      path_rule {
        paths   = path_matcher.value.paths
        service = path_matcher.value.service
      }
    }
  }

  ## TODO somehow dynamically build this based on buckets passed into the module
  depends_on = [
    null_resource.bucket-delay,
    null_resource.default-bucket-delay
  ]

}
