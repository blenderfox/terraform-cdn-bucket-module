resource "google_storage_bucket" "default-bucket" {
  name          = var.default_bucket_name
  location      = "EU"
  force_destroy = true
}

## Give all users read access to the bucket
## Used only because the cdn-fill account doesn't seem to work once this is removed
## Ref: https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_default_object_access_control
resource "google_storage_default_object_access_control" "default-bucket-public-read" {
  bucket = google_storage_bucket.bucket.name
  role   = "READER"
  entity = "allUsers"

  depends_on = [
    google_storage_bucket.bucket,
  ]
}

## Give all owners in the preprod project owner access
resource "google_storage_default_object_access_control" "default-bucket-owner-access" {
  bucket = google_storage_bucket.bucket.name
  role   = "OWNER"
  entity = "project-owners-${var.project}"
  depends_on = [
    google_storage_bucket.bucket
  ]
}

## Give all viewers in the project reader access
## (service-PROJECT_NUM@cloud-cdn-fill.iam.gserviceaccount.com:objectViewer should also be in this group)
resource "google_storage_default_object_access_control" "default-bucket-viewer-access" {
  bucket = google_storage_bucket.bucket.name
  role   = "READER"
  entity = "project-viewers-${var.project}"
}

resource "google_storage_bucket_iam_binding" "default-bucket-cdn-fill-access" {
  bucket = google_storage_bucket.bucket.name
  role   = "roles/storage.objectViewer"
  members = [
    "serviceAccount:service-${var.project_id}@cloud-cdn-fill.iam.gserviceaccount.com",
  ]
}
