module "cdn-router" {
  # source              = "git@gitlab.com:blenderfox/terraform-cdn-bucket-module.git//module"
  source               = "../module"
  project              = var.project
  region               = var.region
  zone                 = var.zone
  project_id           = var.project_id
  domain               = var.domain
  default-cdn-prefix   = var.default-cdn-prefix
  dns-prefix           = var.dns-prefix
  domains              = var.domains
  dns-zone             = var.dns-zone
  public-address-name  = var.public-address-name
  cert-name            = var.cert-name
  sleep_time           = var.sleep_time
  bucket_name          = var.bucket_name
  default_bucket_name  = var.default_bucket_name
  host_rules           = local.host_rules
  path_matcher         = local.path_matcher
  routing-map-name     = var.routing-map-name
  https-proxy-name     = var.https-proxy-name
  forwarding-rule-name = var.forwarding-rule-name
}
