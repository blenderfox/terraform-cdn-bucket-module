locals {
  host_rules = [
    {
      hosts = [
      "${var.default_bucket_name}.${var.domain}"]
      path_matcher = "default"
    },
    {
      hosts = [
      "${var.bucket_name}.${var.domain}"]
      path_matcher = "assets"
    }
  ]

  path_matcher = [
    {
      name            = "default"
      default_service = module.cdn-router.default_bucket_service.self_link
      paths = [
      "/*"]
      service = module.cdn-router.default_bucket_service.self_link
    },
    {
      name            = "assets"
      default_service = module.cdn-router.default_bucket_service.self_link
      paths = [
      "/*"]
      service = module.cdn-router.bucket_service.self_link
    }
  ]
}
