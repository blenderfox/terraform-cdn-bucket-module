project            = "blender-fox"
region             = "europe-west2"
zone               = "europe-west2-a"
project_id         = "355316569954"
dns-prefix         = "test-jo"
domain             = "blenderfox.net"
default-cdn-prefix = "cdn"
domains = [
  "defaultsvc.blenderfox.net",
  "cdn.blenderfox.net",
  "assets.blenderfox.net"
]
dns-zone             = "blender-fox"
public-address-name  = "cdn-ext-ip"
cert-name            = "cdn-cert"
bucket_name          = "jo-test-bucket"
default_bucket_name  = "jo-test-default-bucket"
sleep_time           = "30"
routing-map-name     = "routing-map-preprod"
https-proxy-name     = "https-proxy-preprod"
forwarding-rule-name = "forwarding-rule-preprod"
