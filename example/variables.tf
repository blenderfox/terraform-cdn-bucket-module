variable "project" {}

variable "region" {}

variable "zone" {}

variable "project_id" {}

variable "dns-prefix" {}

variable "domain" {}

variable "default-cdn-prefix" {}

variable "domains" {}

variable "dns-zone" {}

variable "public-address-name" {}

variable "cert-name" {}

variable "sleep_time" {}

variable "bucket_name" {}

variable "default_bucket_name" {}

variable "routing-map-name" {}

variable "https-proxy-name" {}

variable "forwarding-rule-name" {}
